<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculaate</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/Variables.css" />
    <link rel="stylesheet" type="text/css" href="css/index.css" />
</head>

<body>
    <section>
        <img src="https://www.itvscience.com/wp-content/uploads/2016/07/mental-stages-maths.jpg" />
        <div class="container modal_form">
            <div class="row align-items-center flex-column">
                <div class="title">ax<sup>2</sup> + bx + c = 0</div>
                <form action="calculate.php" method="post">
                    <div class="part1">
                        <input type="number" name="a" placeholder="a" id="a" class="form-control" />
                    </div>
                    <div class="part2">
                        <input type="number" name="b" placeholder="b" id="b" class="form-control" />
                    </div>
                    <div class="part3">
                        <input type="number" name="c" placeholder="c" id="c" class="form-control" />
                    </div>
                    <div class="buttons">
                        <button type="button" class="clear btn btn-primary">Clear</button>
                        <input type="submit" value="Calculate" class="btn btn-primary" />
                    </div>
                </form>
                <div class="result">

                </div>
            </div>
        </div>
    </section>
    <script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/ajax.js"></script>
</body>

</html>