<?php 
//Get a, b, c fields.
if ( isset($_POST["a"], $_POST["b"], $_POST["c"]) ) {
    if ( $_POST["a"] == '' || $_POST["b"] == '' || $_POST["c"] == '' ) {
        echo "<div class='error'>Please fill out the missing fields</div>";
    } else {
        $a = $_POST["a"];
        $b = $_POST["b"];
        $c = $_POST["c"];
        if ( $a == 0 ) {
            echo "<div class='error'>You can not trim a 0</div>";
        } else {
            //A $x1 and $x2 this quadratic equation of fօrmula x1 and x2. 
            $x1 = (-$b + ($b ** 2 - 4 * $a * $c)** .5) / (2 * $a);
            $x2 = (-$b - ($b ** 2 - 4 * $a * $c)** .5) / (2 * $a);
            if ( is_nan($x1) || is_nan($x2) ) {
                echo "<div class='error'>Please fill out the fields correctly </div>";
            } else {
                echo "<div class='response'>x<sub>1</sub> = $x1</div>" . "<div class='response'>x<sub>2</sub> = $x2</div>";
            }
        }
    }
}
?>