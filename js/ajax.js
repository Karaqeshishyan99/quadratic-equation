$(document).ready(() => {
	//Send ajax request with jquery
	$('form').on('submit', function() {
		const form = $(this),
			url = form.attr('action'),
			method = form.attr('method'),
			data = {};
		form.find('[name]').each(function(index, value) {
			const input = $(this),
				name = input.attr('name'),
				val = input.val();

			data[name] = val;
		});
		$.ajax({
			url: url,
			type: method,
			data: data,
			success: function(response) {
				$('.result').html(response);
			}
		});
		event.preventDefault();
	});
	//With this function we clear section of result.
	$('.clear').on('click', function() {
		$('#a').val('');
		$('#b').val('');
		$('#c').val('');
		$('.result').text('');
	});
});
